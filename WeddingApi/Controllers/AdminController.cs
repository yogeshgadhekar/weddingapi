﻿using Datahandling.Implementation;
using Datahandling.Interface;
using DataHandlingLayer.Implementation;
using DataHandlingLayer.Interface;
using DataHandlingLayer.UserView;
using DataLayer.Implementation;
using DataLayer.Respositories;
using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Web;
using System.Collections.Specialized;

namespace WeddingApi.Controllers
{
    [Authorize]
    [RoutePrefix("User")]
    public class AdminController : ApiController
    {
        InfAdminGet adminget = new ImplementationAdminGet();
        InfUserGet userget = new ImpUserGet();
        InfUserInsert userinsert = new ImplementUserInsert();
        [Route("getFunction/{weddingid:int}")]
        public Data<FunctionDetailsView> getFunction(int weddingid)
        {
            return new Response<FunctionDetailsView>().getMessege(userget.getFunctionOfUser(weddingid), new FunctionDetailsView());
        }
        [Route("getBrideDetails/{weddingid:int}")]
        public IHttpActionResult getBribe(int weddingid)
        {
            var a = userget.getBrideDetails(weddingid);
            if (a == null)
            {
                return Content(HttpStatusCode.NoContent, a);
            }
            else
            {
                return Content(HttpStatusCode.OK, a);
            }

        }
        [Route("getGroomeDetails/{weddingid:int}")]
        public IHttpActionResult getGroom(int weddingid)
        {
            var a = userget.getGroomDetails(weddingid);
            if (a == null)
            {
                return Content(HttpStatusCode.NoContent, a);
            }
            else
            {
                return Content(HttpStatusCode.OK, a);
            }

        }
        [Route("getWeddingDetails")]
        public WeddingInfo1 getWeddingDetails()
        {
            return userget.getWedding(User.Identity.GetUserId());
        }
        [Route("getFunctionImage/{id:int}/{weddingid:int}")]
        public Data<userFunction> getFunctionImage(int id, int weddingid)
        {
         
            return new Response<userFunction>().getMessege(userget.get(id, weddingid), new userFunction());
        }
        [Route("getFunctionVideo/{id:int}/{weddingid:int}")]
        public IHttpActionResult getFunctionVideo(int id, int weddingid)
        {
            var a = userget.getVideo(id, weddingid);
            if (a == null)
                return Content(HttpStatusCode.NoContent, a);
            else
                return Content(HttpStatusCode.OK, a);
         
        }
        [Route("getFunctionName/{weddingid:int}")]
        public IHttpActionResult getFunctionList(int weddingid)
        {
            var a=adminget.getfunctionList(weddingid);
            if (a == null)
                return Content(HttpStatusCode.NoContent, a);
            else
                return Content(HttpStatusCode.OK, a);
        }
        [Route("addFunctionImage")]
        public IHttpActionResult addFunctionImage()
        {
            FunctionImage fi = new FunctionImage();
            try
            {
                fi.WeddingId= Convert.ToInt32(HttpContext.Current.Request.Form["WeddingId"]);
               // fi.id = Convert.ToInt32(HttpContext.Current.Request.Form["Id"]);
                fi.AddedDate = DateTime.Now;
                fi.AddedDate = DateTime.Now;
                fi.FunctionDetailsId = Convert.ToInt32(HttpContext.Current.Request.Form["FunctionDetailsId"]);
                var httpRequest = HttpContext.Current.Request;
                if (HttpContext.Current.Request.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];                     
                        var filePath = HttpContext.Current.Server.MapPath("~/Content/image/FunctionImage/" + postedFile.FileName);
                        postedFile.SaveAs(filePath);
                        fi.ImageName = postedFile.FileName;
                    }

                }
                if (fi.id == 0)
                {
                    userinsert.insert(fi);
                    return Content(HttpStatusCode.Created, fi);
                }
                else
                {
                    userinsert.update(fi);
                    return Content(HttpStatusCode.Created, fi);
                }
            }
            catch(Exception e)
            {
                e.ToString();
                return Content(HttpStatusCode.BadRequest, fi);
            }

           
           
        }

        [Route("addVideo")]
        public IHttpActionResult addVideo()
        {
            FunctionVideo fi = new FunctionVideo();
            try
            {
                fi.WeddingId = Convert.ToInt32(HttpContext.Current.Request.Form["WeddingId"]);
               // fi.id = Convert.ToInt32(HttpContext.Current.Request.Form["Id"]);
                fi.AddedDate = DateTime.Now;
                fi.AddedDate = DateTime.Now;
                fi.FunctionDetailsId = Convert.ToInt32(HttpContext.Current.Request.Form["FunctionDetailsId"]);
                var httpRequest = HttpContext.Current.Request;
                if (HttpContext.Current.Request.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                       
                        var filePath = HttpContext.Current.Server.MapPath("http://worldindia.in/weddingadmin/Content/video/" + postedFile.FileName);
                        postedFile.SaveAs(filePath);
                        fi.VideoName = postedFile.FileName;
                    }

                }
                if (fi.id == 0)
                {
                    userinsert.insert(fi);
                    return Content(HttpStatusCode.Created, fi);
                }
                else
                {
                    userinsert.update(fi);
                    return Content(HttpStatusCode.Created, fi);
                }
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.BadRequest, fi);
            }
        }

        [Route("getSelectedGift/{weddingid:int}")]
        public IHttpActionResult getSelected(int weddingid)
        {
          var a=  adminget.getSelectedGift(weddingid);
            if (a == null)
                return Content(HttpStatusCode.NoContent, a);
            else
                return Content(HttpStatusCode.OK, a);
        }
    }
    
}
