﻿using Datahandling.Implementation;
using Datahandling.Interface;
using DataHandlingLayer.Implementation;
using DataHandlingLayer.Interface;
using DataHandlingLayer.UserView;
using DataLayer.Implementation;
using DataLayer.Respositories;
using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace WeddingApi.Controllers
{
    [RoutePrefix("Guest")]
    public class GuestController : ApiController
    {
        InfUserGet guestget = new ImpUserGet();
        InfUserInsert userinsert = new ImplementUserInsert();
        [Route("getDashboard")]
        public Data<DashboardMenu> getDashboardMenu()
        {
            return new Response<DashboardMenu>().getMessege(guestget.getDashboard(), new DashboardMenu());
        }
        [Route("guestLogin")]
        public IHttpActionResult guestLogin(InviteGuest ig)
        {
            var guest = guestget.guestLogin(ig.Email, ig.Password, ig.WeddingId);
            if (guest != null)
                return Content(HttpStatusCode.OK, guest);
            else
                return Content(HttpStatusCode.NotFound, guest);
        }
        [Route("getWeddingDetails/{weddingid:int}")]
        public Wedding getWeddingDetails(int weddingid)
        {
            return guestget.getWedding(weddingid);
        }
        [Route("functionDetails/{weddingid:int}/{userid:int}")]
        public IHttpActionResult getFunctionDetials(int weddingid, int userid)
        {
            var a = guestget.getFunctionDetialsbyUser(weddingid, userid);
            if (a != null)
                return Content(HttpStatusCode.OK, a);
            else
                
                return Content(HttpStatusCode.NotFound, a);
        }
        [Route("getFunctionImage/{id:int}")]
        public IHttpActionResult getFunctionImage(int id)
        {
            var a = guestget.getImage(id);
            if (a != null)
                return Content(HttpStatusCode.OK, a);
            else
                return Content(HttpStatusCode.NotFound, a);
        }
        [Route("getFunctionVideo/{id:int}")]
        public IHttpActionResult getFunctionVideo(int id)
        {
            var a = guestget.getVideo(id);
            if (a != null)
                return Content(HttpStatusCode.OK, a);
            else
                return Content(HttpStatusCode.NotFound, a);
        }
        [Route("giftList/{weddingid:int}")]
        public IHttpActionResult getGiftList(int weddingid)
        {
            var a = guestget.getGiftList(weddingid);
            if (a != null)
                return Content(HttpStatusCode.OK, a);
            else
                return Content(HttpStatusCode.NotFound, a);
        }

        [Route("addGift")]
        public IHttpActionResult addGift(SelectedGift g)
        {
            try
            {
                userinsert.insert(g);
                return Content(HttpStatusCode.OK, g);
            }
            catch (Exception e)
            {
                e.ToString();
                return Content(HttpStatusCode.BadRequest, g);
            }
        }
        [Route("removeGift")]
        public IHttpActionResult delete(SelectedGift g)
        {
            try
            {
                userinsert.delete(g);
                return Content(HttpStatusCode.OK, g);
            }
            catch (Exception e)
            {
                e.ToString();
                return Content(HttpStatusCode.BadRequest, g);
            }
        }
        [Route("getSelectedGift")]
        public IHttpActionResult GiftDetails(Gift g)
        {
            var a = guestget.getGift(g.WeddingId, g.UserId, g.groomUserId, g.brideUserId);
            try
            {
                if (a != null)
                    return Content(HttpStatusCode.OK, a);
                else
                    return Content(HttpStatusCode.NotFound, a);
            }
            catch (Exception e)
            {
                e.ToString();
                return Content(HttpStatusCode.BadRequest, a);
            }

        }
        [Route("getBrideDetails/{weddingid:int}")]
        public IHttpActionResult getBribe(int weddingid)
        {
            var a = guestget.getBrideDetails(weddingid);
            if(a==null)
            {
                return Content(HttpStatusCode.NoContent, a);
            }
            else
            {
                return Content(HttpStatusCode.OK, a);
            }
           
        }
        [Route("getGroomeDetails/{weddingid:int}")]
        public IHttpActionResult getGroom(int weddingid)
        {
            var a = guestget.getGroomDetails(weddingid);
            if (a == null)
            {
                return Content(HttpStatusCode.NoContent, a);
            }
            else
            {
                return Content(HttpStatusCode.OK, a);
            }

        }
        [Route("getFamilyDetails/{brideid:int}/{groomid:int}")]
        public IHttpActionResult getFamilyDetails(int brideid,int groomid)
        {
            var a = guestget.getRelative(brideid,groomid);
            if (a == null)
            {
                return Content(HttpStatusCode.NoContent, a);
            }
            else
            {
                return Content(HttpStatusCode.OK, a);
            }

        }
        [Route("Feedback")]
        public IHttpActionResult addFeedBack(FeedBack fd)
        {
            try
            {
                userinsert.insert(fd);
                return Content(HttpStatusCode.Created, fd);
            }
            catch(Exception e)
            {
                e.ToString();
                return Content(HttpStatusCode.BadRequest, fd);
            }
            
        }
        [Route("aboutUs")]
        public IHttpActionResult getAbout(FeedBack fd)
        {
            return Content(HttpStatusCode.OK, guestget.getAbout(1));
        }
        [Route("disclaimer")]
        public IHttpActionResult getdis()
        {
            return Content(HttpStatusCode.OK, guestget.getDis(1));
        }
    }
}
