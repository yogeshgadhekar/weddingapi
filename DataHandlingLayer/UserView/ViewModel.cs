﻿using DBModel;
using System;
using System.Collections.Generic;

namespace DataHandlingLayer.UserView
{

    public class RoleView
    {
        public int RoleId { get; set; }
        public string Role { get; set; }
    }
    public class UserDetals
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public Nullable<System.DateTime> RegisterDate { get; set; }
        public string Status { get; set; }
    }
    public class WeddingInfo1
    {
      
        public int WeddingId { get; set; }
        public string WeddingTitle { get; set; }
        public string WeddingDescription { get; set; }
        public string Venue { get; set; }
        public DateTime? date { get; set; }
        public string GroomImage { get; set; }
        public string BrideImage { get; set; }
        public string Status { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int BrideId { get; set; }
        public int GroomId { get; set; }
    }
    public class GroomDetail
    {
        public int GroomId { get; set; }
        public string Name { get; set; }
        public string Education { get; set; }
        public string Occupation { get; set; }
        public string Highest_Education { get; set; }
        public string PG { get; set; }
        public string UG { get; set; }
        public string Mother_Name { get; set; }
        public string Mom_Occupation { get; set; }
        public string FatherName { get; set; }
        public string Father_Occupation { get; set; }
        public Nullable<int> Brothers { get; set; }
        public string Brothers_Name { get; set; }
        public string Sisters { get; set; }
        public string Sisters_Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Pincode { get; set; }
    }
    public class InvitedGuestViewModel
    {

        public int Id { get; set; }
        public string MobileNo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int WeddingId { get; set; }
        public string Status { get; set; }
        public string Password { get; set; }
        public int BrideId { get; set; }
        public int GroomId { get; set; }
        public bool isInvitedForFunction { get; set; }
        public bool isInvitedForWedding { get; set; }

        public string WeddingTitle { get; set; }

        public string WeddingDescription { get; set; }

        public string Venue { get; set; }

        public DateTime? date { get; set; }

        public string GroomImage { get; set; }

        public string BrideImage { get; set; }
        public DateTime AddedDate { get; set; }

        public string UserId { get; set; }
    }
    public class CityView
    {
        public string CityName { get; set; }
        public int CityId { get; set; }
    }

    public class StateView
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
    }
    public class CountryView
    {
        public string CountryName { get; set; }
        public int CountryId { get; set; }
    }
    public class FunctionView
    {
        public int functionId { get; set; }
        public string functionName { get; set; }
        public int FunctionDetailsId { get; set; }
        public DateTime Date { get; set; }
        public string place { get; set; }
        public int WeddingId { get; set; }
        public string Status { get; set; }
    }
    public class userFunction
    {
        public int FunctionId { get; set; }
        public string FunctionName { get; set; }
        public int Id { get; set; }
        public string Image { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime FunctionDate { get; set; }
    }
    public class invitedGuestView
    {
        public int functionId { get; set; }
        public int FunctionDetailsId { get; set; }
        public string FunctionName { get; set; }
        public string GusetName { get; set; }
        public string Address { get; set; }
        public int WeddingId { get; set; }
        public DateTime FunctionDate { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
    }
    public class WeddingInfo
    {
        public int WeddingId { get; set; }
        public DateTime WeddingDate { get; set; }
        public string title { get; set; }
        public string GroomImage { get; set; }
        public string BrideImage { get; set; }
        public string Venue { get; set; }
    }
    public class FunctionDropdown
    {

        public int Id { get; set; }
        public int functiondetailsid { get; set; }
        public string functionname { get; set; }
    }
    public class FunctionGuestViewModel
    {
        public int Id { get; set; }
        public string FunctionDetailsId { get; set; }
        public string MobileNo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int WeddingId { get; set; }
        public string UserId { get; set; }
        public string FunctionName { get; set; }
        public DateTime FunctionDate { get; set; }

        public string Place { get; set; }
    }
    public class FunctionDetailsView
    {
        public int Id { get; set; }
        public int functionid { get; set; }
        public Function function { get; set; }
        public int Weddingid { get; set; }
        public Wedding wedding { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public string Place { get; set; }
        public string FunctionName { get; set; }
    }

    public class GiftViewModel
    {
        public int Id { get; set; }
        public string GuestName { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public int WeddingId { get; set; }
        public int brideUserId { get; set; }
        public int groomUserId { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime updatedDate { get; set; }
        public int id { get; set; }
        public int userid { get; set; }
        public int weddingId { get; set; }
        public int brideUserid { get; set; }
        public int groomUserid { get; set; }
        public int giftid { get; set; }

    }

  
    public class RelativeView
    {
        public string Name { get; set; }
        public string Relation { get; set; }
        public string ImageName { get; set; }
        public string RelationName { get; set; }
        public CoupleDetails couple { get; set; }        
    }

   
    public class relative
    {
        public PaternalGrandParent paternalgrandParent { get; set; }
        public MaternalGrandParent maternalgrandParent { get; set; }
        public parent parent { get; set; }
        public UncleAndAunt UncleAndAunt { get; set; }
        public Brother brother { get; set; }
        public Sister sister { get; set; }
        public Other other { get; set; }

    }
    public class parent
    {
        public  List<RelativeView> mother { get; set; }
        public List<RelativeView> father { get; set; }
        
    }
    public class PaternalGrandParent
    {
        public List<RelativeView> grandMother { get; set; }
        public List<RelativeView> grandFather { get; set; }
    }
    public class MaternalGrandParent
    {
        public List<RelativeView> grandMother { get; set; }
        public List<RelativeView> grandFather { get; set; }
    }
    public class Brother
    {
        public List<RelativeView> brother { get; set; }
       

    }
    public class Sister
    {
        public List<RelativeView> sister { get; set; }
  
    }

    public class UncleAndAunt
    {
        public List<RelativeView> uncle { get; set; }
       
    }
    public class Other
    {
        public List<RelativeView> other { get; set; }
    }
    public class SelectedGiftView
    {
        public string Image { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }


    }
    public class GroomBrideView
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Education { get; set; }
        public string Occupation { get; set; }
        public string MotherName { get; set; }
        public string FatherName { get; set; }
        public int NumberOfBrother { get; set; }
        public string BrotherNames { get; set; }
        public int NumberOfSister { get; set; }
        public string SisterNames { get; set; }
        public int CountryId { get; set; }
        public string Email { get; set; }
        public int SteteId { get; set; }
        public int CityId { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string BrideImage { get; set; }
        public string GroomImage { get; set; }
    }
}
