﻿using DataAccessLayer;
using Datahandling.Interface;
using DBModel;
using System;
using System.Linq;

namespace Datahandling.Implementation
{
    public class ImplementUserInsert : InfUserInsert
    {
           IBaseRepository<Bride> bride = new BaseRepository<Bride>();
           IBaseRepository<Groom> groom = new BaseRepository<Groom>();
           IBaseRepository<FunctionDetails> functiondetails = new BaseRepository<FunctionDetails>();
           IBaseRepository<Wedding> wedding = new BaseRepository<Wedding>();
           IBaseRepository<InviteGuest> guest = new BaseRepository<InviteGuest>();
           IBaseRepository<FunctionImage> fimage = new BaseRepository<FunctionImage>();
           IBaseRepository<FunctionVideo> functionvideo = new BaseRepository<FunctionVideo>();
           IBaseRepository<WeddingGuest> weddingguest = new BaseRepository<WeddingGuest>();
           IBaseRepository<FunctionGuest> functionguest = new BaseRepository<FunctionGuest>();
           IBaseRepository<UserDetails> userdetails = new BaseRepository<UserDetails>();
        IBaseRepository<SelectedGift> selectedgift = new BaseRepository<SelectedGift>();
        IBaseRepository<FeedBack> feedback = new BaseRepository<FeedBack>();
        IBaseRepository<Gift> gift = new BaseRepository<Gift>();
        public int addFunction()
        {
            throw new NotImplementedException();
        }
        public int insert(FunctionImage fi)
        {
            try
            {
                fimage.Insert(fi);
                return 1;
            }
            catch(Exception e)
            {
                e.ToString();
                return 0;
            }
           
        }
        public int addFunctionVideo()
        {
            throw new NotImplementedException();
        }
        public void addGiftDetails(Gift g)
        {
            if (g.Id == 0)
              gift.Insert(g);
            else
              gift.Update(g, g.Id);
        }

        public void insert(SelectedGift g)
        {
            if (g.id == 0)
            {
                selectedgift.Insert(g);
            }
            else
            {
                SelectedGift sf = selectedgift.FindBy(m => m.id == g.id).SingleOrDefault();
                sf.giftid = g.giftid;
                sf.groomUserid = g.groomUserid;
                sf.brideUserid = g.brideUserid;
                selectedgift.Update(sf, g.id);
            }
        }
        public void delete(SelectedGift g)
        {
                selectedgift.Delete(g);  
        }
        public int insert(Wedding wd)
        {
            wedding.Insert(wd);
            return wd.WeddingId;
        }
        public int insert(Groom gd)
        {
            try
            {
                groom.Insert(gd);
                return 1;
            }
            catch(Exception e)
            {
                e.ToString();
                return 0;
            }
         
        }

        public void insert(FunctionVideo video)
        {
            video.AddedDate = DateTime.Now;
           
            functionvideo.Insert(video);
        }
        public void update(FunctionVideo video)
        {
            FunctionVideo fv = functionvideo.FindBy(m => m.id == video.id).SingleOrDefault();
            fv.VideoName = video.VideoName;
            fv.FunctionDetailsId = video.FunctionDetailsId;
            functionvideo.Update(fv, video.id);
        }
        public int insert(Bride gd)
        {
            try
            {
                bride.Insert(gd);
                return 1;
            }
            catch (Exception e)
            {
                e.ToString();
                return 0;
            }

        }

        public void update(FunctionImage img)
        {
            FunctionImage fi = fimage.FindBy(m => m.id == img.id).SingleOrDefault();
            if(img.ImageName !=null)
            {
                fi.ImageName = img.ImageName;
            }           
            fi.FunctionDetailsId = img.FunctionDetailsId;
            fimage.Update(fi, img.id);
        }
        public int update(Groom gr)
        {
            try
            {
                Groom grm = groom.FindBy(m => m.Id == gr.Id).SingleOrDefault();
                grm.MotherName = gr.MotherName;
                grm.Name = gr.Name;
                grm.NumberOfBrother = gr.NumberOfBrother;
                grm.NumberOfSister = gr.NumberOfSister;
                grm.Occupation = gr.Occupation;
                grm.SisterNames = gr.SisterNames;
                grm.BrotherNames = gr.BrotherNames;
                grm.CityId = gr.CityId;
                grm.ContactNo = gr.ContactNo;
                grm.CountryId = gr.CountryId;
                grm.Education = gr.Education;
                grm.FatherName = gr.FatherName;
                grm.Address = gr.Address;               
                groom.Update(grm, gr.Id);
                return 1;
            }
            catch(Exception e)

            {
                e.ToString();
                return 0;
            }
        }
        public int update(Bride br)
        {
            try
            {
                Bride grm = bride.FindBy(m => m.Id == br.Id).SingleOrDefault();
                grm.MotherName = br.MotherName;
                grm.Name = br.Name;
                grm.NumberOfBrother = br.NumberOfBrother;
                grm.NumberOfSister = br.NumberOfSister;
                grm.Occupation = br.Occupation;
                grm.SisterNames = br.SisterNames;
                grm.BrotherNames = br.BrotherNames;
                grm.CityId = br.CityId;
                grm.ContactNo = br.ContactNo;
                grm.CountryId = br.CountryId;
                grm.Education = br.Education;
                grm.FatherName = br.FatherName;
                grm.Address = br.Address;
                bride.Update(grm, br.Id);
                return 1;
            }
            catch (Exception e)
            {
                e.ToString();
                return 0;
            }
        }
        public void insert(FunctionDetails fd)
        {
            
            functiondetails.Insert(fd);
        }
        public int addGuest()
        {
            throw new NotImplementedException();
        }
        public  void update(Wedding wd)
        {
            Wedding modelwedding = wedding.FindBy(m => m.WeddingId == wd.WeddingId).SingleOrDefault();


            if(wd.BrideImage !=null)
            {
                modelwedding.BrideImage = wd.BrideImage;
            }
            if(wd.GroomImage !=null)
            {
                modelwedding.GroomImage = wd.GroomImage;
            }                    
            modelwedding.WeddingDescription = wd.WeddingDescription;
            modelwedding.WeddingTitle = wd.WeddingTitle;
            modelwedding.Venue = wd.Venue;
            modelwedding.date = wd.date;
            int id = wd.WeddingId;
            wedding.Update(modelwedding, id); 
        }
        public int insert(InviteGuest guestdata)
        {
            try
            {
                guestdata.Status = "Active";
                guest.Insert(guestdata);
                return 1;
            }
            catch(Exception e)           
            {
                e.ToString();
                return 0;
            }
            
        }
        
        public int insert(WeddingGuest wg)
        {
            try {
                weddingguest.Insert(wg);
                return 1;
            }
            catch(Exception e)
            {
                e.ToString();
                return 0;
            }
        }
        public int insert(FunctionGuest fg)
        {
            try
            {
                functionguest.Insert(fg);
                return 1;
            }
            catch (Exception e)
            {
                e.ToString();
                return 0;
            }
        }
        public int insert(UserDetails ud)
        {
            try
            {
                userdetails.Insert(ud);
                return 1;
            }
            catch(Exception e)
            {
                e.ToString();
                return 0;
            }

        }
        public int insert()
        {
            throw new NotImplementedException();
        }
        public void insert(FeedBack fd)
        {
            fd.AddedDate = DateTime.Now;
            fd.Status = "Active";
            feedback.Insert(fd);

        }
        
    }
}
