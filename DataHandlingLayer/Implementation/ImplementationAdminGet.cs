﻿using DataAccessLayer;
using DataHandlingLayer.AdminView;
using DataHandlingLayer.Interface;
using DataHandlingLayer.UserView;
using DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
namespace DataHandlingLayer.Implementation
{
   
    public class ImplementationAdminGet : InfAdminGet
    {
       
        IBaseRepository<City> city = new BaseRepository<City>();
        IBaseRepository<State> state = new BaseRepository<State>();
        IBaseRepository<Country> country = new BaseRepository<Country>();
        IBaseRepository<Function> function = new BaseRepository<Function>();
        IBaseRepository<FunctionDetails> functiondetails = new BaseRepository<FunctionDetails>();
        IBaseRepository<SelectedGift> selectedgift = new BaseRepository<SelectedGift>();
        IBaseRepository<Gift> gift = new BaseRepository<Gift>();
        IBaseRepository<AboutUs> aboutus = new BaseRepository<AboutUs>();
        IBaseRepository<Disclaimer> disclaimer = new BaseRepository<Disclaimer>();
        //IBaseRepository<AspNetRole> s = new BaseRepository<AspNetRole>();
        //IBaseRepository<AspNetUser> user = new BaseRepository<AspNetUser>();
        GetUserDetails det = new GetUserDetails();
        public List<RoleView> getRole()
        {
         
            return new List<RoleView>();
            //return (from r in role.GetAll()
            //        select new RoleView
            //        {
            //            Role = r.Name,
            //            RoleId = Convert.ToInt32(r.Id)
            //        }).ToList();
            
        }
        //get Userid
        public string getUserId(string username)
        {
            return "ac";
            //return user.FindBy(s => s.UserName == username).Select(s => s.Id).ToString();
        }
        public List<CountryView> getCountry()
        {
            return (from s in country.GetAll()
                    select new CountryView
                    {
                        CountryId = s.CountryId,
                        CountryName = s.CountryName
                    }).ToList();
        }
        public List<StateView> getState(int coutryid)
        {
            return (from s in state.FindBy(s => s.CountryId == coutryid)
                    select new StateView
                    {
                        StateId = s.StateId,
                        StateName = s.StateName
                    }).ToList();
        }
        public List<CityView> getCity(int stateid)
        {
            return (from c in city.FindBy(c => c.StateId == stateid)
                    select new CityView
                    {
                        CityName = c.CityName,
                        CityId = c.CityId
                    }).ToList();
        }
        public List<AdminFunctionView> getFunctionList()
        {
            return (from f in function.GetAll()
                    select new AdminFunctionView
                    {
                        FunctionName = f.FunctionName,
                        Description = f.FunctionDescription,
                        id = f.Id,
                       // AddedBy =det.getUsername(f.AddedBy),
                        AddedDate = f.AddedDate,
                        ModifiedBy = f.ModifiedBy,
                        ModifiedDate = Convert.ToDateTime(f.ModifiedDate),
                        Status = f.Status
                    }
                    ).ToList();           
        }

        public List<Function> getfunctionList(int weddingid)
        {
            return (from fd in functiondetails.FindBy(f => f.Weddingid == weddingid && f.Status==1)
                    join f in function.FindBy(s => s.Status == "Active") on fd.functionid equals f.Id
                    select new Function
                    {
                        AddedDate=fd.Date,
                        FunctionName = f.FunctionName,
                        Id = fd.Id
                    }).ToList();
        }
        string imagepath = "http://worldindia.in/weddingadmin/Content/image/GiftImage/";
        public   List<SelectedGiftView> getSelectedGift(int weddingid)
        {

            return (from g in gift.FindBy(m => m.WeddingId == weddingid && m.Status == "Active")
                    join gg in selectedgift.FindBy(s=>s.weddingId==weddingid) on g.Id equals gg.giftid
                    
                    select new SelectedGiftView
                    {
                        Image = imagepath + "" + g.Image,
                        Title = g.Title,
                        Url = g.Url
                    }).ToList();
        }
       
    }
}
