﻿using DataHandlingLayer.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBModel;
using DataAccessLayer;
using DataHandlingLayer.UserView;
using System.Web;

namespace DataHandlingLayer.Implementation
{
    public class ImpUserGet : InfUserGet
    {
        IBaseRepository<Function> function = new BaseRepository<Function>();
        IBaseRepository<FunctionDetails> functiondetails = new BaseRepository<FunctionDetails>();
        IBaseRepository<FunctionImage> functionimage = new BaseRepository<FunctionImage>();
        IBaseRepository<FunctionVideo> functionvideo = new BaseRepository<FunctionVideo>();
        IBaseRepository<Wedding> wedding = new BaseRepository<Wedding>();
        IBaseRepository<Groom> groom = new BaseRepository<Groom>();
        IBaseRepository<Bride> bride = new BaseRepository<Bride>();
        IBaseRepository<InviteGuest> inviteguest = new BaseRepository<InviteGuest>();
        IBaseRepository<FunctionGuest> functionguest = new BaseRepository<FunctionGuest>();
        IBaseRepository<Gift> gift = new BaseRepository<Gift>();
        IBaseRepository<DashboardMenu> dashboard = new BaseRepository<DashboardMenu>();
        IBaseRepository<SelectedGift> selectedgift = new BaseRepository<SelectedGift>();
        IBaseRepository<FamilyMember> familymember = new BaseRepository<FamilyMember>();
        IBaseRepository<Couple> couple = new BaseRepository<Couple>();
        IBaseRepository<Relation> relation = new BaseRepository<Relation>();
        IBaseRepository<UserDetals> userdetails = new BaseRepository<UserDetals>();
        IBaseRepository<UserDetails> userd = new BaseRepository<UserDetails>();
        IBaseRepository<CoupleDetails> coupledetails = new BaseRepository<CoupleDetails>();
        IBaseRepository<AboutUs> aboutus = new BaseRepository<AboutUs>();
        IBaseRepository<Disclaimer> disclaimer = new BaseRepository<Disclaimer>();

        string host = HttpContext.Current.Request.Url.Host;
        public List<FunctionDropdown> getFunctionDetails(int weddingid)
        {
            return (from f in functiondetails.FindBy(m => m.Weddingid == weddingid)
                    join ff in function.GetAll() on f.functionid equals ff.Id
                    select new FunctionDropdown
                    {
                        Id = f.Id,
                        functionname = ff.FunctionName
                    }
                      ).ToList();
        }


        public WeddingInfo1 getWedding1(string userid)
        {
            int a = getWeddingId(userid);
            return (from w in wedding.FindBy(u => u.UserId == userid)
                    join u in userd.GetAll() on w.UserId equals u.UserId
                    join g in groom.GetAll() on w.WeddingId equals g.WeddingId
                    join b in bride.GetAll() on w.WeddingId equals b.WeddingId
                    select new WeddingInfo1
                    {
                        BrideImage = w.BrideImage,
                        GroomImage = w.GroomImage,
                        WeddingDescription = w.WeddingDescription,
                        WeddingTitle = w.WeddingTitle,
                        WeddingId = w.WeddingId,
                        UserId = w.UserId,
                        Venue = w.Venue,
                        date = w.date,
                        Name = u.FullName,
                        Email = u.Email,
                        BrideId=b.Id,
                        GroomId=g.Id
                     
                    }).SingleOrDefault();
        }


        public WeddingInfo1 getWedding(string userid)
        {
            int a = getWeddingId(userid);
            return (from w in wedding.FindBy(u => u.UserId == userid)
                    join u in userd.GetAll() on w.UserId equals u.UserId
                    join g in groom.FindBy(gg=>gg.WeddingId==a) on w.WeddingId equals g.WeddingId
                    join b in bride.FindBy(b => b.WeddingId == a) on w.WeddingId equals b.WeddingId
                    select new WeddingInfo1
                    {                        
                        BrideImage = w.BrideImage,
                        GroomImage = w.GroomImage,
                        WeddingDescription = w.WeddingDescription,
                        WeddingTitle = w.WeddingTitle,
                        WeddingId = w.WeddingId,
                        UserId = w.UserId,
                        Venue = w.Venue,
                        date = w.date,
                        Name=u.FullName,
                        Email=u.Email,
                        BrideId=b.Id,
                        GroomId=g.Id
                    }).SingleOrDefault();
        }
        public Wedding getWedding(int id)
        {
            return (from w in wedding.FindBy(u => u.WeddingId == id)
                    select new Wedding
                    {
                        BrideImage = w.BrideImage,
                        GroomImage = w.GroomImage,
                        WeddingDescription = w.WeddingDescription,
                        WeddingTitle = w.WeddingTitle,
                        WeddingId = w.WeddingId,
                        UserId = w.UserId,
                        Venue = w.Venue,
                        date = w.date,
                    }).SingleOrDefault();
        }
        public int getWeddingId(string userid)
        {
            return wedding.FindBy(s => s.UserId == userid).Select(s => s.WeddingId).SingleOrDefault();
        }
        public int isVailable(int weddingid)
        {
            return wedding.FindBy(s => s.WeddingId == weddingid).Select(s => s.WeddingId).SingleOrDefault();
        }
        string imagepath = "http://worldindia.in/weddingadmin/Content/image/GiftImage/";
        public List<Gift> getGiftList(int weddingid)
        {
            return (from g in gift.FindBy(m => m.WeddingId == weddingid && m.Status == "Active")
                    select new Gift
                    {
                        Id=g.Id,
                        Image = imagepath + "" + g.Image,
                        Title = g.Title,
                        Url = g.Url
                    }).ToList();
        }
        string path1 = "http://worldindia.in/weddingadmin/Content/image/GroomAndBride/";
        public GroomBrideView getGroomDetails(int weddingid)
        {
            var groommodel = (from g in groom.FindBy(u => u.WeddingId == weddingid)
                              join w in wedding.GetAll() on g.WeddingId equals w.WeddingId
                              select new GroomBrideView
                              {
                                  CityId = g.CityId,
                                  SteteId = g.SteteId,
                                  CountryId = g.CityId,
                                  Name = g.Name,
                                  Id = g.Id,
                                  Occupation = g.Occupation,
                                  Education = g.Education,
                                  MotherName = g.MotherName,
                                  FatherName = g.FatherName,
                                  ContactNo = g.ContactNo,
                                  GroomImage = path1 + "" + w.GroomImage,
                                  Address=g.Address,
                                  Email=g.email

                              }).SingleOrDefault();

            return groommodel;
        }
        public GroomBrideView getBrideDetails(int weddingid)
        {
            var bridemodel = (from b in bride.FindBy(u => u.WeddingId == weddingid)
                              join w in wedding.GetAll() on b.WeddingId equals w.WeddingId
                              select new GroomBrideView
                              {
                                  CityId = b.CityId,
                                  SteteId = b.SteteId,
                                  CountryId = b.CityId,
                                  Name = b.Name,
                                  Id = b.Id,
                                  Occupation = b.Occupation,
                                  Education = b.Education,
                                  MotherName = b.MotherName,
                                  FatherName = b.FatherName,
                                  ContactNo = b.ContactNo,
                                  BrideImage = path1 + "" + w.BrideImage,
                                  Address=b.Address,
                                 Email=b.email
                              }).SingleOrDefault();

            return bridemodel;
        }
        public List<userFunction> getFunction(int weddingid)
        {
            return (from f in function.GetAll()
                    join ff in functiondetails.FindBy(c => c.Weddingid == weddingid) on f.Id equals ff.functionid
                    select new userFunction
                    {
                        FunctionId = Convert.ToInt32(ff.Id),
                        FunctionName = f.FunctionName
                    }
                    ).ToList();
        }


        string path2 = "http://worldindia.in/weddingadmin/Content/image/FunctionImage/";
        public List<userFunction> get(int id, int weddingid)
        {
            return (from fd in functiondetails.FindBy(f => f.Id == id && f.Weddingid == weddingid)

                    join ff in function.GetAll() on fd.functionid equals ff.Id
                    join fi in functionimage.FindBy(m=>m.Status==1) on fd.Id equals fi.FunctionDetailsId
                    select new userFunction
                    {
                        FunctionId = fd.Id,
                        Image = path2+""+fi.ImageName
                    }
                    ).ToList();

        }

        string pathv = "http://worldindia.in/weddingadmin/Content/image/FunctionImage/";
        public List<FunctionVideo> getVideo(int id, int weddingid)
        {
            return (from fd in functiondetails.FindBy(f => f.Id == id && f.Weddingid == weddingid && f.Status==1)
                    join ff in function.GetAll() on fd.functionid equals ff.Id
                    join fi in functionvideo.GetAll() on fd.Id equals fi.FunctionDetailsId
                    select new FunctionVideo
                    {
                        id = fd.Id,
                        VideoName = pathv + ""+fi.VideoName,VideoTitle=fi.VideoTitle
                    }
                    ).ToList();
        }
        string groomi = "url";
        string bridei = "url";
        public List<WeddingInfo> weddingInfo(int weddingid)
        {
            return (from w in wedding.FindBy(ww => ww.WeddingId == weddingid)
                    join g in groom.GetAll() on w.WeddingId equals g.WeddingId
                    join b in bride.GetAll() on w.WeddingId equals b.WeddingId
                    select new WeddingInfo
                    {
                        WeddingId = w.WeddingId,
                        WeddingDate = Convert.ToDateTime(w.date),
                        GroomImage = groomi + w.GroomImage,
                        BrideImage = bridei + w.BrideImage,
                        title = w.WeddingDescription,
                        Venue = w.Venue
                    }).ToList();
        }
        public List<InviteGuest> getInvitedGuest(int weddingid)
        {
            var a = inviteguest.FindBy(c => c.WeddingId == weddingid).ToList();
            return a;
        }
        public List<FunctionGuestViewModel> getfunctionInvitedGuest(int weddingid)
        {
            var a = (from f in functionguest.FindBy(c => c.WeddingId == weddingid)
                         // join fd in functiondetails.GetAll() on f.WeddingId equals fd.Weddingid  
                         //  join ff in function.GetAll() on fd.functionid equals ff.Id                
                     select new FunctionGuestViewModel
                     {
                         //  Name=f.Name,
                         // Email=f.Email,
                         //  MobileNo=f.MobileNo,
                         //     FunctionName=getSplit(f.FunctionDetailsId.Split(','))
                     }).ToList();
            return a;
        }
        public string getSplit(string[] abc)
        {
            string s = "";
            int i = 0;
            while (abc.Length > i)
            {

                int id = Convert.ToInt32(abc[i]);
                s += (from f in function.GetAll() join fd in functiondetails.FindBy(ss => ss.Id == id) on f.Id equals fd.functionid select f.FunctionName).SingleOrDefault();
                s += ",";
                i++;
            }
            return s;
        }
        public List<FunctionDetailsView> getFunctionOfUser(int weddingid)
        {
            var a = (from fd in functiondetails.FindBy(w => w.Weddingid == weddingid && w.Status==1)
                     join f in function.GetAll() on fd.functionid equals f.Id
                     select new FunctionDetailsView
                     {
                         FunctionName = f.FunctionName,
                         Id = fd.Id,
                         functionid = f.Id,
                         Date = fd.Date,
                         Time = Convert.ToDateTime(fd.Time.ToShortTimeString()),
                         Place = fd.Place
                     }).ToList();
            return a;

        }
        string path = "http://worldindia.in/weddingadmin/Content/Image/IconImage/";
        public List<DashboardMenu> getDashboard()
        {

            return (from d in dashboard.FindBy(c => c.Status == "Active")
                    select new DashboardMenu
                    {
                        Name = d.Name,
                        Image = path + "" + d.Image,
                    }).ToList();

        }
        public InvitedGuestViewModel guestLogin(string email, string password, int weddingid)
        {
            return (from inv in inviteguest.FindBy(m => m.Email == email && m.Password == password && m.WeddingId == weddingid && m.Status=="Active")
                    join g in groom.GetAll() on inv.WeddingId equals g.WeddingId
                    join b in bride.GetAll() on inv.WeddingId equals b.WeddingId
                    join w in wedding.GetAll() on inv.WeddingId equals w.WeddingId
                    select new InvitedGuestViewModel
                    {
                        WeddingDescription = w.WeddingDescription,
                        WeddingTitle = w.WeddingTitle,
                        BrideImage = w.BrideImage,
                        GroomImage = w.GroomImage,
                        Venue = w.Venue,
                        date = w.date,
                        BrideId = b.Id,
                        GroomId = g.Id,
                        WeddingId = inv.WeddingId,
                        Id = inv.Id,
                        isInvitedForWedding = inv.isInvitedForWedding,
                        isInvitedForFunction = inv.isInvitedForFunction,
                        Name=inv.Name,
                        Email=inv.Email
                    }).SingleOrDefault();
        }
        public List<FunctionGuestViewModel> getFunctionDetialsbyUser(int weddingid, int userid)
        {
            var a = (from fd in functiondetails.FindBy(w => w.Weddingid == weddingid && w.Status==1)
                     join f in function.GetAll() on fd.functionid equals f.Id
                     join fg in functionguest.FindBy(g => g.UserId == userid) on fd.Id equals fg.FunctionDetailsId
                     select new FunctionGuestViewModel
                     {
                         FunctionName = f.FunctionName,
                         Id = fd.Id,
                         FunctionDate = fd.Date,
                         Place = fd.Place,
                     }).ToList();
            return a;
        }
        string ipatgh = "http://worldindia.in/weddingadmin/Content/image/FunctionImage/";
        public List<FunctionImage> getImage(int id)
        {
            var a = (from fd in functiondetails.GetAll()
                     join f in function.GetAll() on fd.functionid equals f.Id
                     join fi in functionimage.FindBy(fimg => fimg.FunctionDetailsId == id) on fd.Id equals fi.FunctionDetailsId
                     select new FunctionImage
                     {
                         ImageName = ipatgh+""+fi.ImageName,
                     }).ToList();
            return a;
        }
        string videopath = "http://worldindia.in/weddingadmin/Content/Video/";
        public List<FunctionVideo> getVideo(int id)
        {
            var a = (from fd in functiondetails.FindBy(w => w.Id == id)
                     join f in function.GetAll() on fd.functionid equals f.Id
                     join fi in functionvideo.FindBy(fimg => fimg.FunctionDetailsId == id) on fd.Id equals fi.FunctionDetailsId
                     select new FunctionVideo
                     {                         
                         VideoName = videopath + "" + fi.VideoName,
                     }).ToList();
            return a;
        }
        string giftpath = "http://worldindia.in/weddingadmin/Content/image/GiftImage/";
        public List<GiftViewModel> getGift(int weddingid, int userid, int groomid, int brideid)
        {

            return (from g in selectedgift.FindBy(s => s.weddingId == weddingid && s.userid == userid)
                    join u in inviteguest.GetAll() on g.userid equals u.Id
                    join f in gift.GetAll() on g.giftid equals f.Id
                    select new GiftViewModel
                    {
                        GuestName=u.Name,
                        Image = giftpath + "" + f.Image,
                        Url = f.Url,
                        Title = f.Title,
                        id = g.id
                    }).ToList();

        }
        public string FileUpload(string path, HttpPostedFileBase fu)
        {
            if (fu != null)
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + path.Replace("/", "\\") + fu.FileName;
                string ImageDirectoryFP = path.Replace("/", "\\");
                string ImageDirectory = "~/" + path;
                string ImagePath = "~/" + path + fu.FileName;
                string fileNameWithExtension = System.IO.Path.GetExtension(fu.FileName);
                string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(fu.FileName);
                string ImageName = fu.FileName;
                int iteration = 1;
                while (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(ImagePath)))
                {
                    ImagePath = string.Concat(ImageDirectory, fileNameWithoutExtension, "-", iteration, fileNameWithExtension);
                    filePath = string.Concat(ImageDirectoryFP, fileNameWithoutExtension, "-", iteration, fileNameWithExtension);
                    ImageName = string.Concat(fileNameWithoutExtension, "-", iteration, fileNameWithExtension);
                    iteration += 1;
                }
                if (iteration == 1)
                {
                    fu.SaveAs(filePath);
                }
                else
                {
                    fu.SaveAs(AppDomain.CurrentDomain.BaseDirectory + filePath);
                }
                return ImageName;
            }
            else
            {
                return null;
            }

        }
        public string FileUpload1(string path, HttpPostedFile fu)
        {
            if (fu != null)
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + path.Replace("/", "\\") + fu.FileName;
                string ImageDirectoryFP = path.Replace("/", "\\");
                string ImageDirectory = "~/" + path;
                string ImagePath = "~/" + path + fu.FileName;
                string fileNameWithExtension = System.IO.Path.GetExtension(fu.FileName);
                string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(fu.FileName);
                string ImageName = fu.FileName;
                int iteration = 1;
                while (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(ImagePath)))
                {
                    ImagePath = string.Concat(ImageDirectory, fileNameWithoutExtension, "-", iteration, fileNameWithExtension);
                    filePath = string.Concat(ImageDirectoryFP, fileNameWithoutExtension, "-", iteration, fileNameWithExtension);
                    ImageName = string.Concat(fileNameWithoutExtension, "-", iteration, fileNameWithExtension);
                    iteration += 1;
                }
                if (iteration == 1)
                {
                    fu.SaveAs(filePath);
                }
                else
                {
                    fu.SaveAs(AppDomain.CurrentDomain.BaseDirectory + filePath);
                }
                return ImageName;
            }
            else
            {
                return null;
            }

        }
        public int getCoupleid(int id)
        {
            return couple.FindBy(m => m.first_person_id == id || m.second_person_id == id).Select(m => m.id).SingleOrDefault();
        }
        string pathfamily = "http://worldindia.in/weddingadmin/Content/Image/FamilyImage/";
        public relative getRelative(int brideid, int groomid)
        {
            relative rel = new relative();
            parent parent = new parent();
            rel.parent = new parent
            {
                mother = getrr(6, brideid, groomid)
            ,
                father = getrr(5, brideid, groomid)
            };
            rel.maternalgrandParent = new MaternalGrandParent
            {
                grandFather = getrr(2, brideid, groomid),
                grandMother = getrr(1, brideid, groomid)
            };
            rel.paternalgrandParent = new PaternalGrandParent
            {
                grandFather = getrr(4, brideid, groomid),
                grandMother = getrr(3, brideid, groomid)
            };
            rel.brother = new Brother
            {
                brother = getrr(7, brideid, groomid),
                
            };
            rel.sister = new Sister
            {              
                sister = getrr(8, brideid, groomid)
            };
            rel.UncleAndAunt = new UncleAndAunt
            {
                uncle = getrr(10, brideid, groomid),
               // aunt = getrr(11, brideid, groomid)
            };
            rel.other = new Other
            {
                other = getrr(9, brideid, groomid),
               
            };
            return rel;
            //if (brideid != 0)
            //{
            //    return (from f in familymember.FindBy(m => m.brideId == brideid)
            //            join ff in familymember.GetAll() on f.Id equals ff.coupleid = ff.Id
            //            join r in relation.GetAll() on f.relation_id equals r.Id
            //            select new RelativeView
            //            {
            //                Name = ff.Name,
            //                Relation = r.name,
            //                CoupleId = getCoupleid(Convert.ToInt32(ff.coupleid)),
            //                parentId = Convert.ToInt32(f.parent_id),
            //                ImageName = pathfamily + "" + f.image,
            //            }).ToList();
            //}
            //else
            //{
            //    return (from f in familymember.FindBy(m => m.groomid == groomid)
            //            join ff in familymember.GetAll() on f.Id equals ff.coupleid = ff.Id
            //            join r in relation.GetAll() on f.relation_id equals r.Id
            //            select new RelativeView
            //            {
            //                Name = ff.Name,
            //                Relation = r.name,
            //                CoupleId = getCoupleid(Convert.ToInt32(ff.coupleid)),
            //                parentId = Convert.ToInt32(f.parent_id),
            //                ImageName = pathfamily + "" + f.image,
            //            }).ToList();
            //}
        }

      

        public List<RelativeView> getrr(int rid,int brideid, int groomid)
        {
           


            if (brideid != 0)
            {
                return (from f in familymember.FindBy(m => m.brideId == brideid && m.relation_id==rid && m.status=="Active")
                      
                        join r in relation.GetAll() on f.relation_id equals r.Id
                        select new RelativeView
                        {
                            Name = f.Name,
                            Relation = r.name,
                            //CoupleId = getCoupleid(Convert.ToInt32(ff.coupleid)),
                            //parentId = Convert.ToInt32(f.parent_id),
                            ImageName = pathfamily + "" + f.image,
                            RelationName = r.name  ,
                            couple= getCouple((int)f.coupleid)
                        }).ToList();
            }
            else
            {
                return (from f in familymember.FindBy(m => m.groomid == groomid && m.relation_id == rid && m.status == "Active")
                      
                        join r in relation.GetAll() on f.relation_id equals r.Id
                        select new RelativeView
                        {
                            Name = f.Name,
                            Relation = r.name,
                            //CoupleId = getCoupleid(Convert.ToInt32(ff.coupleid)),
                            //parentId = Convert.ToInt32(f.parent_id),
                            ImageName = pathfamily + "" + f.image,
                            RelationName = r.name,
                            couple = getCouple((int)f.coupleid)
                        }).ToList();
            }
        }
        public CoupleDetails getCouple(int id)
        {
            return (from f in coupledetails.FindBy(m => m.Id==id)
                  
                    select new CoupleDetails
                    {
                        Name = f.Name,                    
                        //CoupleId = getCoupleid(Convert.ToInt32(ff.coupleid)),
                        //parentId = Convert.ToInt32(f.parent_id),
                        ImageName = pathfamily + "" + f.ImageName,
                        RelationName = f.RelationName
                    }).SingleOrDefault();
        }
        string pathabout = "http://worldindia.in/weddingadmin/Content/Image/AboutUs/";
        public AboutUs getAbout(int id)
        {
            var ab = (from a in aboutus.GetAll()
                     select new AboutUs
                     {
                         Image = pathabout + "" + a.Image,
                         Text = a.Text

                     }
                   ).SingleOrDefault();
            return ab;
        }
        public Disclaimer getDis(int id)
        {
            return disclaimer.GetAll().SingleOrDefault();
        }

    }
}
