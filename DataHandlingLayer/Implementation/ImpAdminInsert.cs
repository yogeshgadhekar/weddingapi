﻿using DataAccessLayer;
using DataHandlingLayer.Interface;
using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataHandlingLayer.Implementation
{
    public class ImpAdminInsert: infAdminInsert
    {
        IBaseRepository<Function> function = new BaseRepository<Function>();
        IBaseRepository<UserDetails> userdetails = new BaseRepository<UserDetails>();
        public void addFunction(Function fun)
        {
            function.Insert(fun);
        }

        public void addUserDetails(UserDetails user)
        {
            userdetails.Insert(user);
        }
        
    }
}
