﻿using System;
using System.Data.Entity;
using System.Collections.Generic;

using System.Linq;


namespace DataAccessLayer
{
    /// <summary>
    /// Base class for all SQL based service classes
    /// </summary>
    /// <typeparam name="T">The domain object type</typeparam>
    /// <typeparam name="TU">The database object type</typeparam>
    public class BaseRepository<T> : IBaseRepository<T>
        where T : class
    {
        private readonly IUnitOfWork _unitOfWork = new UnitOfWork();
        internal DbSet<T> dbSet;
        public BaseRepository()
        {
            //if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
            //_unitOfWork = unitOfWork;
            this.dbSet = _unitOfWork.Db.Set<T>();
        }

        /// <summary>
        /// Returns the object with the primary key specifies or throws
        /// </summary>
        /// <typeparam name="TU">The type to map the result to</typeparam>
        /// <param name="primaryKey">The primary key</param>
        /// <returns>The result mapped to the specified type</returns>
        public T Single(object primaryKey)
        {
            var dbResult = dbSet.Find(primaryKey);
            return dbResult;
        }





        /// <summary>
        /// Returns the object with the primary key specifies or the default for the type
        /// </summary>
        /// <typeparam name="TU">The type to map the result to</typeparam>
        /// <param name="primaryKey">The primary key</param>
        /// <returns>The result mapped to the specified type</returns>
        public T SingleOrDefault(object primaryKey)
        {
            var dbResult = dbSet.Find(primaryKey);
            return dbResult;
        }




        public bool Exists(object primaryKey)
        {
            return dbSet.Find(primaryKey) == null ? false : true;
        }

        public virtual void Insert(T entity)
        {
            dynamic obj = dbSet.Add(entity);
            this._unitOfWork.Db.SaveChanges();
            //return obj.Id;

        }


        public virtual void Update(T entity, long key)
        {
            if (entity != null)
            {
                T existing = _unitOfWork.Db.Set<T>().Find(key);

                if (existing != null)
                {
                    _unitOfWork.Db.Entry(existing).CurrentValues.SetValues(entity);
                    _unitOfWork.Db.SaveChanges();
                }
            }
        }

        public virtual void UpdateString(T entity, string key)
        {
            if (entity != null)
            {
                T existing = _unitOfWork.Db.Set<T>().Find(key);

                if (existing != null)
                {
                    _unitOfWork.Db.Entry(existing).CurrentValues.SetValues(entity);
                    _unitOfWork.Db.SaveChanges();
                }
            }
        }

        public void Delete(T entity)
        {
            if (_unitOfWork.Db.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dynamic obj = dbSet.Remove(entity);
            this._unitOfWork.Db.SaveChanges();
            //return obj.Id;
        }

        public IUnitOfWork UnitOfWork { get { return _unitOfWork; } }
        internal DbContext Database { get { return _unitOfWork.Db; } }
        public Dictionary<string, string> GetAuditNames(dynamic dynamicObject)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetAll()
        {
            return dbSet.AsEnumerable().ToList();
        }

            
        public IEnumerable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            IEnumerable<T> query = dbSet.Where(predicate).AsEnumerable();
            return query;
        }

        public T SingleRow(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            T query = dbSet.Where(predicate).AsEnumerable().SingleOrDefault();
            return query;
        }

        public int Count(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            int rowCount = dbSet.Where(predicate).AsEnumerable().Count();
            return rowCount;
        }



    }
}
