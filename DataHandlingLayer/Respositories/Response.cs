﻿using DataLayer.Respositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Implementation
{
    public class Response<T>
    {
        public Data<T> getMessege(List<T> data,T t)
        {
            if(data.Count==0)
            {
                return new Data <T>{ Messege = "Data Not Found", success = false, status = 404, response = data };
            }
            else
            {
                return new Data<T> { Messege = "Success", success = true, status = 200, response = data };
            }
        }
    }
}
