﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Respositories
{
   public class Data<T>
    {
        public bool success { get; set; }
        public string Messege { get; set; }
        public int status { get; set; }
        public List<T> response { get; set; }
    }
}
