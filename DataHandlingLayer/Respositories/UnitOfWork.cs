﻿
using DBModel;
using Model;
using System.Data.Entity;
using System.Transactions;


namespace DataAccessLayer
{
    public class UnitOfWork : IUnitOfWork
    {
        private TransactionScope _transaction;
        private readonly  weddingEntities _db;


        public UnitOfWork()
        {
            _db = new weddingEntities();
        }

        public void Dispose()
        {

        }

        public void StartTransaction()
        {
            _transaction = new TransactionScope();
        }

        public void Commit()
        {
            _db.SaveChanges();
            _transaction.Complete();
        }

        public DbContext Db
        {
            get { return _db; }
        }
    }
}
