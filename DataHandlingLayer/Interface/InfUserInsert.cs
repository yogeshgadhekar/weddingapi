﻿


using DBModel;

namespace Datahandling.Interface
{
    public interface InfUserInsert
    {
        int insert();
        int insert(Groom gd);
        int insert(Bride gd);
        int addFunction();      
        int addFunctionVideo();
        void addGiftDetails(Gift g);        
        void insert(FunctionDetails fd);
        int insert(Wedding wd);
        void update(Wedding wd);
        int update(Groom groom);
        int update(Bride bride);
        int insert(InviteGuest guest);
        int insert(FunctionImage image);
        void insert(FunctionVideo fv);
        int insert(WeddingGuest wg);
        int insert(FunctionGuest fg);
        int insert(UserDetails ud);
        void update(FunctionImage img);
        void update(FunctionVideo video);
        void insert(SelectedGift g);
        void delete(SelectedGift g);
        void insert(FeedBack fd);
    }
}
