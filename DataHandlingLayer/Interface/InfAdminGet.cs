﻿


using DataHandlingLayer.AdminView;
using DataHandlingLayer.UserView;
using DBModel;
using System.Collections.Generic;


namespace DataHandlingLayer.Interface
{
    public interface InfAdminGet
    {
        List<RoleView> getRole();
        List<StateView> getState(int coutryid);
        List<CityView> getCity(int stateid);
        List<CountryView> getCountry();
        List<AdminFunctionView> getFunctionList();
        List<Function> getfunctionList(int weddingid);
        List<SelectedGiftView> getSelectedGift(int weddingid);
       
    }
}
