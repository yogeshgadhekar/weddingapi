﻿using DataHandlingLayer.UserView;
using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DataHandlingLayer.Interface
{
    public interface InfUserGet
    {
        string FileUpload1(string path, HttpPostedFile fu);
        List<FunctionDropdown> getFunctionDetails(int weddingid);
        WeddingInfo1 getWedding(string userid);
        GroomBrideView getGroomDetails(int weddingid);
        int getWeddingId(string userid);
        GroomBrideView getBrideDetails(int weddingid);
        List<userFunction> getFunction(int weddingid);
        string FileUpload(string path, HttpPostedFileBase fu);
        List<userFunction> get(int id, int weddingid);
        List<FunctionVideo> getVideo(int id, int weddingid);
        List<FunctionGuestViewModel> getfunctionInvitedGuest(int weddingid);
        /// <summary>
        /// mobile user 
        int isVailable(int weddingid);
        //List<userFunction> userFunctionDetails();
        List<WeddingInfo> weddingInfo(int id);
        List<InviteGuest> getInvitedGuest(int weddingid);
        List<FunctionDetailsView> getFunctionOfUser(int weddingid);
        InvitedGuestViewModel guestLogin(string email,string password,int weddingid);
        List<FunctionGuestViewModel> getFunctionDetialsbyUser(int weddingid, int userid);
        List<FunctionImage> getImage(int id);
        List<FunctionVideo> getVideo(int id);
        List<GiftViewModel> getGift(int weddingid,int userid,int groomid,int brideid);
        List<DashboardMenu> getDashboard();
        Wedding getWedding(int id);
        List<Gift> getGiftList(int weddingid);
        relative getRelative(int brideid, int groomid);
        AboutUs getAbout(int id);
        Disclaimer getDis(int id);
        WeddingInfo1 getWedding1(string userid);
    }
}
