namespace Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class wedding : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TblBride",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Education = c.String(),
                        Occupation = c.String(),
                        MotherName = c.String(),
                        FatherName = c.String(),
                        NumberOfBrother = c.Int(nullable: false),
                        BrotherNames = c.String(),
                        NumberOfSister = c.Int(nullable: false),
                        SisterNames = c.String(),
                        CountryId = c.Int(nullable: false),
                        SteteId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                        Address = c.String(),
                        ContactNo = c.String(),
                        WeddingId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Weddings", t => t.WeddingId, cascadeDelete: true)
                .Index(t => t.WeddingId);
            
            CreateTable(
                "dbo.Weddings",
                c => new
                    {
                        WeddingId = c.Int(nullable: false, identity: true),
                        WeddingTitle = c.String(nullable: false),
                        WeddingDescription = c.String(nullable: false),
                        Venue = c.String(nullable: false),
                        date = c.DateTime(nullable: false),
                        GroomImage = c.String(),
                        BrideImage = c.String(),
                        AddedDate = c.DateTime(nullable: false),
                        Status = c.String(),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.WeddingId);
            
            CreateTable(
                "dbo.FunctionGuests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FunctionDetailsId = c.Int(nullable: false),
                        WeddingId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Weddings", t => t.WeddingId, cascadeDelete: true)
                .Index(t => t.WeddingId);
            
            CreateTable(
                "dbo.tblFunctionDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        functionid = c.Int(nullable: false),
                        Weddingid = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Place = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tblfunction", t => t.functionid, cascadeDelete: true)
                .ForeignKey("dbo.Weddings", t => t.Weddingid, cascadeDelete: true)
                .Index(t => t.functionid)
                .Index(t => t.Weddingid);
            
            CreateTable(
                "dbo.tblfunction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FunctionName = c.String(),
                        FunctionDescription = c.String(),
                        AddedBy = c.String(),
                        AddedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                        ModifiedDate = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tblGroom",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Education = c.String(),
                        Occupation = c.String(),
                        MotherName = c.String(),
                        FatherName = c.String(),
                        NumberOfBrother = c.Int(nullable: false),
                        BrotherNames = c.String(),
                        NumberOfSister = c.Int(nullable: false),
                        SisterNames = c.String(),
                        CountryId = c.Int(nullable: false),
                        SteteId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                        Address = c.String(),
                        ContactNo = c.String(),
                        WeddingId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Weddings", t => t.WeddingId, cascadeDelete: true)
                .Index(t => t.WeddingId);
            
            CreateTable(
                "dbo.InviteGuests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MobileNo = c.String(),
                        Name = c.String(),
                        Email = c.String(),
                        WeddingId = c.Int(nullable: false),
                        Status = c.String(),
                        Password = c.String(),
                        isInvitedForFunction = c.Boolean(nullable: false),
                        isInvitedForWedding = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Weddings", t => t.WeddingId, cascadeDelete: true)
                .Index(t => t.WeddingId);
            
            CreateTable(
                "dbo.tblCity",
                c => new
                    {
                        CityId = c.Int(nullable: false, identity: true),
                        CityName = c.String(),
                        StateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CityId);
            
            CreateTable(
                "dbo.tblCountry",
                c => new
                    {
                        CountryId = c.Int(nullable: false, identity: true),
                        CountryName = c.String(),
                    })
                .PrimaryKey(t => t.CountryId);
            
            CreateTable(
                "dbo.tblCouple",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        first_person_id = c.Int(nullable: false),
                        second_person_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.DashboardMenus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Image = c.String(nullable: false),
                        AddedBy = c.String(),
                        UpdatedBy = c.String(),
                        AddedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tblFamilyMember",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        Name = c.String(),
                        image = c.String(),
                        maritial_id = c.Int(nullable: false),
                        weddingid = c.Int(nullable: false),
                        age = c.Int(nullable: false),
                        relation_id = c.Int(nullable: false),
                        parent_id = c.Int(nullable: false),
                        visibility_status = c.String(),
                        status = c.String(),
                        wedPersonid = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FunctionImages",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ImageName = c.String(),
                        WeddingId = c.Int(nullable: false),
                        FunctionDetailsId = c.Int(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.FunctionVideos",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        VideoName = c.String(),
                        WeddingId = c.Int(nullable: false),
                        FunctionDetailsId = c.Int(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Gifts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WeddingId = c.Int(nullable: false),
                        Url = c.String(),
                        brideUserId = c.Int(nullable: false),
                        groomUserId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tblMaritialStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tblRelationType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        gender = c.String(),
                        status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tblState",
                c => new
                    {
                        StateId = c.Int(nullable: false, identity: true),
                        StateName = c.String(),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StateId);
            
            CreateTable(
                "dbo.UserDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        FullName = c.String(),
                        ContactNo = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InviteGuests", "WeddingId", "dbo.Weddings");
            DropForeignKey("dbo.tblGroom", "WeddingId", "dbo.Weddings");
            DropForeignKey("dbo.tblFunctionDetails", "Weddingid", "dbo.Weddings");
            DropForeignKey("dbo.tblFunctionDetails", "functionid", "dbo.tblfunction");
            DropForeignKey("dbo.FunctionGuests", "WeddingId", "dbo.Weddings");
            DropForeignKey("dbo.TblBride", "WeddingId", "dbo.Weddings");
            DropIndex("dbo.InviteGuests", new[] { "WeddingId" });
            DropIndex("dbo.tblGroom", new[] { "WeddingId" });
            DropIndex("dbo.tblFunctionDetails", new[] { "Weddingid" });
            DropIndex("dbo.tblFunctionDetails", new[] { "functionid" });
            DropIndex("dbo.FunctionGuests", new[] { "WeddingId" });
            DropIndex("dbo.TblBride", new[] { "WeddingId" });
            DropTable("dbo.UserDetails");
            DropTable("dbo.tblState");
            DropTable("dbo.tblRelationType");
            DropTable("dbo.tblMaritialStatus");
            DropTable("dbo.Gifts");
            DropTable("dbo.FunctionVideos");
            DropTable("dbo.FunctionImages");
            DropTable("dbo.tblFamilyMember");
            DropTable("dbo.DashboardMenus");
            DropTable("dbo.tblCouple");
            DropTable("dbo.tblCountry");
            DropTable("dbo.tblCity");
            DropTable("dbo.InviteGuests");
            DropTable("dbo.tblGroom");
            DropTable("dbo.tblfunction");
            DropTable("dbo.tblFunctionDetails");
            DropTable("dbo.FunctionGuests");
            DropTable("dbo.Weddings");
            DropTable("dbo.TblBride");
        }
    }
}
