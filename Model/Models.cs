﻿

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DBModel
{
    public class UserDetails
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
    }

    public class Disclaimer
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Heading { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string AddedBy { get; set; }
    }
    public class AboutUs
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public string AddedBy { get; set; }
    }
    [Table("tblCountry")]
    public class Country
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }
    [Table("tblState")]
    public class State
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
    }
    [Table("tblCity")]
    public class City
    {
        //  [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int StateId { get; set; }
    }
    public class Wedding
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WeddingId { get; set; }
        [Required(ErrorMessage = "Please enter wedding title")]
        [Display(Name = "Wedding Title")]
        public string WeddingTitle { get; set; }
        [Required(ErrorMessage = "Please enter wedding description")]
        [Display(Name = "Wedding Description")]
        public string WeddingDescription { get; set; }
        [Required(ErrorMessage = "Please enter venue")]
        public string Venue { get; set; }
        [Required(ErrorMessage = "Please Enter Date")]
        public DateTime? date { get; set; }
        [Display(Name = "Select Groom Image")]
        public string GroomImage { get; set; }
        [Display(Name = "Select Bride Image")]
        public string BrideImage { get; set; }
        public DateTime AddedDate { get; set; }
        public string Status { get; set; }
        public string UserId { get; set; }
        public ICollection<FunctionDetails> functiondetails { get; set; }
        public ICollection<Bride> bride { get; set; }
        public ICollection<Groom> groom { get; set; }
        public ICollection<InviteGuest> iguest { get; set; }
        public ICollection<FunctionGuest> fguest { get; set; }
    }
    [Table("TblBride")]
    public class Bride
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please specify education")]
        public string Education { get; set; }
        public string Occupation { get; set; }
        [Required(ErrorMessage = "Please enter mother name")]
        public string MotherName { get; set; }
        [Required(ErrorMessage = "Please specify father name")]
        public string FatherName { get; set; }
        public int NumberOfBrother { get; set; }
        public string BrotherNames { get; set; }
        public int NumberOfSister { get; set; }
        public string SisterNames { get; set; }
        public int CountryId { get; set; }
        public int SteteId { get; set; }
        public int CityId { get; set; }
        [Required(ErrorMessage = "Please enter address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please enter email address")]
        public string email { get; set; }
        [Required(ErrorMessage = "Please enter contact number")]
        public string ContactNo { get; set; }
        public int WeddingId { get; set; }
        [ForeignKey("WeddingId")]
        public virtual Wedding wedding { get; set; }
    }
    [Table("tblGroom")]
    public class Groom
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please specify education")]
        public string Education { get; set; }
        public string Occupation { get; set; }
        [Required(ErrorMessage = "Please enter mother name")]
        public string MotherName { get; set; }
        [Required(ErrorMessage = "Please specify father name")]
        public string FatherName { get; set; }
        public int NumberOfBrother { get; set; }
        public string BrotherNames { get; set; }
        public int NumberOfSister { get; set; }
       
        public string SisterNames { get; set; }
        public int CountryId { get; set; }
        public int SteteId { get; set; }
        public int CityId { get; set; }
        [Required(ErrorMessage = "Please enter address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please enter email address")]
        public string email { get; set; }
        [Required(ErrorMessage = "Please enter contact number")]
        public string ContactNo { get; set; }

        public int WeddingId { get; set; }
        [ForeignKey("WeddingId")]
        public virtual Wedding wedding { get; set; }
    }
    [Table("tblfunction")]
    public class Function
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string FunctionName { get; set; }
        public string FunctionDescription { get; set; }
        public string AddedBy { get; set; }
        public DateTime AddedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Status { get; set; }
        public virtual List<FunctionDetails> function { get; set; }
    }
    [Table("tblFunctionDetails")]
    public class FunctionDetails
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage ="Please select function")]
        public int functionid { get; set; }
        [ForeignKey("functionid")]
        public Function function { get; set; }
        public int Weddingid { get; set; }
        [ForeignKey("Weddingid")]
        public Wedding wedding { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        [Required(ErrorMessage = "Please enter place")]
        public string Place { get; set; }
        public int Status { get; set; } 
    }
    public class InviteGuest
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage ="Please enter mobile no")]
        public string MobileNo { get; set; }
        [Required(ErrorMessage =" Please enter name")]
        public string Name { get; set; }
        [Required(ErrorMessage = " Please enter emails")]
        public string Email { get; set; }
        public int WeddingId { get; set; }
        [ForeignKey("WeddingId")]
        public virtual Wedding wedding { get; set; }
        public string Status { get; set; }
        public string Password { get; set; }
        public bool isInvitedForFunction { get; set; }
        public bool isInvitedForWedding { get; set; }



    }
    public class FunctionImage
    {   
        public int id { get; set; }
        [Required(ErrorMessage = "Please select image")]
        public string ImageName { get; set; }
        public int WeddingId { get; set; }
        [Required(ErrorMessage = "Please select function")]
        public int FunctionDetailsId { get; set; }
        public DateTime AddedDate { get; set; }

        public int Status { get; set; }

    }
    public class FunctionVideo
    {
        [Key]
        public int id { get; set; }
       
        public string VideoName { get; set; }
        public int WeddingId { get; set; }
        [Required(ErrorMessage = "Please select function")]
        public int FunctionDetailsId { get; set; }
        public DateTime AddedDate { get; set; }
        public string VideoTitle{get;set;}

    }
    public class GuestFunction
    {
        public int Id { get; set; }
        public int FunctionId { get; set; }
        [ForeignKey("FunctionId")]
        public Function function { get; set; }
        public int WeddingId { get; set; }
        [ForeignKey("WeddingId")]
        public Wedding wedding { get; set; }
        public int NumberOfMember { get; set; }
    }
    public class WeddingGuest
    {
        public int Id { get; set; }
        public string userId { get; set; }
        public int WeddingId { get; set; }
        [ForeignKey("WeddingId")]
        public Wedding wedding { get; set; }
    }

    public class FunctionGuest
    {
        public int Id { get; set; }
        public int FunctionDetailsId { get; set; }
        public int WeddingId { get; set; }
        [ForeignKey("WeddingId")]
        public Wedding wedding { get; set; }
        public int UserId { get; set; }

    }
    public class CoupleDetails
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please select image")]
        public string ImageName { get; set; }
        [Required(ErrorMessage = "Please enter relation name")]
        public string RelationName { get; set; }
    }
    [Table("tblFamilyMember")]
    public class FamilyMember
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string image { get; set; }
        public string RelationName { get; set; }
        public int? maritial_id { get; set; }
        public int weddingid { get; set; }
        public int age { get; set; }
        public int relation_id { get; set; }
        public int? parent_id { get; set; }
        public int? coupleid { get; set; }
        public string status { get; set; }
        public int? brideId { get; set; }
        public int? groomid { get; set; }
        public bool isOfBride{get;set;}
        public bool isOfGroom { get; set; }

    }
    [Table("tblCouple")]
    public class Couple
    {
        public int id { get; set; }
        public int first_person_id { get; set; }
        public int second_person_id { get; set; }
    }
    [Table("tblRelationType")]
    public class Relation
    {

        public int Id { get; set; }
        public string name { get; set; }
        public string gender { get; set; }
        public string status { get; set; }

    }
    [Table("tblMaritialStatus")]
    public class MaritialStatus
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
    }


    public class Gift
    {
        public int Id { get; set; }
        public string Image { get; set; }
        [Required(ErrorMessage = "Please enter title")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Please enter URL")]
        public string Url { get; set; }
        public int WeddingId { get; set; }
        public int brideUserId { get; set; }
        public int groomUserId { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime updatedDate { get; set; }
    }
    public class SelectedGift
    {
        public int id { get; set; }
        public int userid { get; set; }
        public int weddingId { get; set; }
        public int brideUserid { get; set; }
        public int groomUserid { get; set; }
        public int giftid { get; set; }
        [ForeignKey("giftid")]
        public Gift gift { get; set; }
    }
    public class DashboardMenu
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please select file")]
        public string Image { get; set; }
        public string AddedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Status { get; set; }

    }

    
    public class FeedBack
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public DateTime AddedDate { get; set; }
    }
    public class GetUserDetails
    {

        //public string getUsername(string id)
        //{
        //    var user = UserManager.FindById(User.Identity.GetUserId());
        //    var context = new IdentityDbContext();
        //    var users = context.Users.Where(s=>s.Id==id).Select(s=>s.UserName).SingleOrDefault();          
        //    return users;
        //}

    }

}
