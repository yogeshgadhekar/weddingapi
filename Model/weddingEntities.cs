﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class weddingEntities : DbContext
    {
        public weddingEntities()
            : base("abc")
        {

        }

        public DbSet<SelectedGift> selectedgift { get; set; }
        public DbSet<Country> country { get; set; }
        public DbSet<State> state { get; set; }
        public DbSet<City> city { get; set; }
        public DbSet<Wedding> wedding { get; set; }
        public DbSet<Bride> bride { get; set; }
        public DbSet<Groom> groom { get; set; }
        public DbSet<Function> function { get; set; }
        public DbSet<FunctionDetails> functiondetails { get; set; }
        public DbSet<InviteGuest> guest { get; set; }
        public DbSet<UserDetails> userdetails { get; set; }
        public DbSet<FunctionVideo> functionvideo { get; set; }
        public DbSet<FamilyMember> familymember { get; set; }
        public DbSet<Couple> couple { get; set; }
        public DbSet<Relation> relation { get; set; }
        public DbSet<FunctionGuest> functionguest { get; set; }
        public DbSet<MaritialStatus> maritialstatus { get; set; }
        public DbSet<Gift> gift { get; set; }
        public DbSet<DashboardMenu> dashboardmenu { get; set; }
        public DbSet<FeedBack> feedback { get; set; }
        public DbSet<CoupleDetails> coupledetails { get; set; }
        public DbSet<Disclaimer> disclaimer { get; set; }
        public DbSet<AboutUs> aboutus { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        public System.Data.Entity.DbSet<DBModel.FunctionImage> FunctionImages { get; set; }
    }
    public class MyDbContextInitializer : DropCreateDatabaseIfModelChanges<weddingEntities>
    {
        protected override void Seed(weddingEntities dbContext)
        {
            // seed data

            base.Seed(dbContext);
        }
    }
}
